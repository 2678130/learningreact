# Building a react app

## Getting Started with our environment

### 1- Installing and setting up a create-react-app environment
- Basic script => `npx create-react-app nameOfApp`
- We can add a template - Progressive Web App (PWA) => `cra-template-pwa` - PWA using typescript => `cra-template-pwa-typescript` - Typescript => `typescript`
- exemple: `npx create-react-app nameOfApp --template cra-template-pwa`

### 2- Quick note on app development
- React uses a subset of javascript called JSX. Browsers like chrome or firefox cannot compile JSX. During development, we'll use a local server that transpile (read converts) our JSX into JS. create-react-app uses Babel.
- During development we'll run the script `npm start`, the webpage created by that script is unoptimized.
- Once our app is complete and ready for production we'll run the script `run build`, that script will create optimized code in a new folder called build.
- In order to create that optimized build create-react-app uses a plugin called webpack which reads through our code and bundles it into optimized JS.

### 3- Install Dependencies
- React does not provide a full fledge app development environment, third party plugins are required or functions need to be added to make it complete.
- The first dependency required for our app routing is react-router-dom.
- `npm install --save react-router-dom`
- `npm install --save-dev @types/react-router-dom` // intellisense
- We'll add further dependencies as we'll go.

## The basics

### 1- The files and what they do ...
- `index.js` => entry point, it has a render function and binds our `<App/>` component to the html page.
- `App.js` => root Component, in almost every situation every other Components will be composed within the App component
- `service-worker.js` `serviceWorkerRegistration.js` => Used when we create a cra-template-pwa, used to create an offline service worker required to build/install a PWA
- `public/index.html` => Our html file, within that file there is a `<div class="root"></div>` where the render function binds our
- `public/manifest.json` => Mainly for Progressive Web App PWA.
- `App.test.js` => For test driven development (TDD), we can write ou own test and see if they pass.

### 2- React basic code
#### 2.1- A React functional component 
```javascript
/**
 * 1- A react component name must be capitalized
 * 2- A react component has a param called props
 * 3- A react component must return some html or another component
 * 4- A react component cannot return separate html, it must always be wrapped in a SINGLE html element
 *      INVALID => return(<div></div>   <main></main>) 
 *      VALID   => return(<main> <div></div> </main>)
 */
const BasicComponent = (props) => {
    return(
        <div>
            Some other html or some other component
            <SomeOtherComponent/>
        </div>
    )
}
```
#### 2.2 Further syntax
```javascript
<Component propretyName={someValue}/> // this syntax is essentially the same as below

let props = {propretyName:someValue}  // this syntax is essentially the same as above
Component(props);

/**
 * javascript values can be written in html when they are within {}
 */
const Component = () => {
    let someArray = [1, 2, 3]

    return (
        <div>
            {
                //I can write javascript in this html when I'm in brackets
                console.log("will log something to the console")

                //using array.map I can return some more html within
                someArray.map(values => {
                    return(
                    <div>{values}</div>
                )})
            }
        </div>
    )
}

```
#### 2.3 About props and composition
- `props` is the default value passed to a functional component, it has a default property called children. React is built on composition over inheritance.
- Read [Composition over inheritance from React Docs](https://reactjs.org/docs/composition-vs-inheritance.html)
```javascript
// we can do this... we destructure props
const AH1Component = ({children}) => <h1>{children}</h1>
```

### 2- Start by adding some folders
- `views`
- `views/components`
- `parts`
- `state`
- `img`



- importing images
- importing css
- saving state
- rendering
- 

### 3- Routing
- We'll route our app with react-router-dom. React creates a Single Page Application (SPA). When requested with a get request, our application will route to the main page. Further navigation within the browser is handle by react-route-dom and is synthetic. For example going from a Homepage to a Products page will not require a network request. The browser and react will render the correct page.
