import React from 'react';
import "./ProductCard.css";
import { GlassContainer } from "../containers/GlassContainer";
import cart from "../../img/cart.svg";

export const ProductCard = ({ imgPath, productName, productId, productPrice, onAdd }) => {

    function handleClick(event) {
        if (onAdd && typeof onAdd === "function") {
            onAdd(event);
        }
    }

    return (
        <GlassContainer style={{padding:".7rem"}}>
            <div className="product-card-img">
                <img src={imgPath} alt="" />
            </div>
            <div className="product-card-info">
                    <div className="product-card-price">
                        {`${(productPrice / 100).toFixed(2)} $`}
                    </div>
                    <div className="product-card-btn" id={productId} onClick={handleClick}>
                        <img src={cart} alt="cart_button"/>
                        <div>
                        Ajouter au panier
                        </div>
                    </div>
            </div>
        </GlassContainer>
    )
}
