import React from 'react';
import "./Containers.css";

export const GlassContainer = ({children, style}) => {
    return (
        <div className="default-glass-container" style={style}>
            {children}
        </div>
    )
}

