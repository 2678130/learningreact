import React from 'react';
import "./Containers.css";

export  const MainContainer = ({children, variant}) => {
    return (
        <main className="default-main">
            {children}
        </main>
    )
}
