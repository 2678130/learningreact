import React  from 'react';
import "./Badge.css";

export const Badge = ({children, number}) => {

    return (
        <div className="default-badge">
            {(number > 0) && <div className="default-badge-bubble">{number}</div>}
            {children}
        </div>
    )
}
