import React from 'react';
import { useHistory } from 'react-router-dom';
import "./Buttons.css";

export const ButtonLink = ({ onClick, children, name, className, path, style }) => {
    if (!name) name = "";
    if (!className) className = "button-link";

    let history = useHistory();


    const handleClick = (event) => {
        if (path && typeof path === "string"){
            history.push(path);
            return;
        }
        if (onClick && typeof onClick === "function") {
            onClick(event);
            return;
        }
        console.log("Error, undefined function handler or improper type");
    }

    return (
        <button style={style} className={className} onClick={handleClick}>
            {children}
            {name}
        </button>
    )
}

export const Button = ({ onClick, children, name, className,  style }) => {
    if (!name) name = "";
    if (!className) className = "default-button";


    const handleClick = (event) => {
        if (onClick && typeof onClick === "function") {
            onClick(event);
            return;
        }
        console.log("Error, undefined function handler or improper type");
    }

    return (
        <button style={style} className={className} onClick={handleClick}>
            {children}
            {name}
        </button>
    )
}
