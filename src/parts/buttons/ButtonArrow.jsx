import React, { FunctionComponent, MouseEvent } from "react";
//import Left from "../../../../img/arrow_left.svg";
//import Right from "../../../../img/arrow_right.svg";
import "./Buttons.css";

/*
interface IButtonArrow {
    name?: string;
    direction: "left" | "right" | "up" | "down";
    onClick: Function;
}
*/

export const ButtonArrow = ({
    name,
    direction,
    onClick,
}) => {
    function handleClick(event) {
        onClick(event);
    }
    return (
        <>
            <button className="default-btn-arrow" onClick={handleClick} name={name}>
                {direction === "left" && <img src={"Left"} alt="arrow_left" />}
                {direction === "right" && <img src={"Right"} alt="arrow_right" />}
            </button>
        </>
    );
};
