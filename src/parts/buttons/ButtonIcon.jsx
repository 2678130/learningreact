import React from 'react';
import DeleteSvg from "../../img/delete.svg";
import EditSvg from "../../img/edit.svg";
import "./Buttons.css";

/*
interface IButtonIcon{
    onClick?:Function;
    name?:string;
    type:"delete" | "edit";
}*/
export const ButtonIcon= ({onClick, name, type}) => {
    function handleClick(event){
        if(onClick){
            onClick(event);
        }
    }
    return (
        <>
            <button className="default-icon-button" onClick={handleClick} name = {name}>
                {type === "delete" && <img src={DeleteSvg} alt="delete_button"/>}
                {type === "edit" && <img src={EditSvg} alt="edit_button"/>}
            </button>
        </>
    )
}
