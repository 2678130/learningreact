import React, { useState } from 'react'
import { CartController } from '../data/CartController';
import { ProductCard } from '../parts/cards/ProductCard'
import { MainContainer } from '../parts/containers/MainContainer'
import "./ViewProducts.css";
import close from "../img/close.svg";

const cartController = new CartController();

export default function Products() {
    const [productName, setProductName] = useState(undefined);
    const products = cartController.getProducts();

    const handleAddProduct = (event) => {
        let productId = event.currentTarget.id;
        cartController.addItemToCart({ productId: productId, qte: 1 });
        const cartItem = cartController.getCartItem(productId)
        add(cartItem.productName);
    }

    /**
     * hack for snackbar
     * @param {string} name 
     */
    const add = (name) =>{
        setProductName(name);
        setTimeout(()=>{
            setProductName(undefined);
        },5000)
    }

    return (
        <MainContainer>
            <div className="product-display">
                {products.map((product, index) => {
                    return <ProductCard productName={product.productName} productId={product.productId} productPrice={product.productPrice} imgPath={product.img} key={index} onAdd={handleAddProduct} />
                })}
            </div>
            <Snackbar product={productName}/>
        </MainContainer>
    )
}


const Snackbar = ({ product }) => {
    const [className, setClassName] = useState("");

    const remove = () => {
        setClassName("hidden");
    }
    if(!product){
        return null;
    }
    return (
        <div className={`product-snackbar ${className}`}>
            <div className={`product-snackbar-item`} >
                {product} a été ajouté au panier.
                <div className="product-snackbar-close" onClick={remove}>
                    <img src={close} alt="close_snackbar" />
                </div>
            </div>
        </div>
    )
}