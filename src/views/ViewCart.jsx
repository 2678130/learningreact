import React, { useState, useEffect } from 'react';
import { CartController } from '../data/CartController';
import { GlassContainer } from '../parts/containers/GlassContainer';
import { MainContainer } from '../parts/containers/MainContainer';
import "./ViewCart.css";
import del from "../img/delete.svg";
import add from "../img/addfull.svg";
import minus from "../img/minus.svg";

export default function Cart() {
    const cartController = new CartController();
    const [items, setItems] = useState(cartController.getCartItems());
    const [total, setTotal] = useState(cartController.getCartTotal());

    if (items.length === 0) {
        setItems([0]);
    }

    const style = {
        display: "flex",
        flexDirection: "column",
        minWidth: "22rem"
    }

    useEffect(() => { // TODO update cart on click

    }, [])

    return (
        <MainContainer>
            <GlassContainer style={style}>
                <div className="cart-top">
                    <h2>Cart</h2>
                    {items.map((item, index) => {
                        return <CartItem key={index} itemId={item.productId} />
                    })}
                </div>
                <div className="cart-bottom">
                    <span style={{ fontSize: "1.4rem" }}>Total: {`${(total / 100).toFixed(2)} $`}</span>
                    <button>Payez</button>
                </div>
            </GlassContainer>
        </MainContainer>
    )
}

const CartItem = ({ itemId }) => {
    const cartController = new CartController();
    const cartItem = cartController.getCartItem(itemId);

    const handleDelete = (event) => {
        event.stopPropagation();
        const { id } = event.currentTarget;
        cartController.deleteCartItem(id)
    }
    const handleAdd = (event) => {
        event.stopPropagation();
        const { id } = event.currentTarget;
        cartController.increaseCartItem(id);
    }
    const handleDecrease = (event) => {
        event.stopPropagation();
        const { id } = event.currentTarget;
        cartController.decreaseCartItem(id);
    }

    if (!itemId) {
        return (<div className="cart-item">
            <div className="cart-item-info" >
                <span style={{ marginLeft: "1rem", fontSize: "1.7rem" }}>Le panier est vide</span>
            </div>
        </div>)
    }

    return (
        <div className="cart-item">
            <div className="cart-item-img-circle">
                <img src={cartItem.img} alt="" />
            </div>
            <div className="cart-item-info">
                <div className="cart-item-name">
                    <span>{cartItem.productName}</span>
                    {`${(cartItem.productPrice * cartItem.qte / 100).toFixed(2)} $`}
                </div>
                <div className="cart-item-option">
                    <div style={{ padding: "0 2rem", display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center" }}>
                        <div style={{ fontSize: "1.2rem", padding: ".5rem 0" }}>
                            Qte : {cartItem.qte}
                        </div>
                        <div style={{ display: "flex", justifyContent: "space-between", width: "4.4rem" }}>
                            <div className="cart-item-modify-qte" id={itemId} onClick={handleDecrease}><img src={minus} alt="less" /></div><div className="cart-item-modify-qte" id={itemId} onClick={handleAdd}><img src={add} alt="plus" /></div>
                        </div>
                    </div>
                    <div className="cart-item-modify-qte" onClick={handleDelete} id={itemId} style={{ height: "min-content" }}>
                        <img src={del} alt="delete" className="cart-delete" />
                    </div>
                </div>
            </div>
        </div>
    )
}