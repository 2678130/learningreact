import React, { useEffect, useState } from 'react'
import { CartController } from '../../data/CartController';
import { ButtonLink } from '../../parts/buttons/Buttons'
import { Badge } from '../../parts/other/Badge';
import logo from "../../img/logo.png"
import "./Header.css"


export default function Header() {
    const cartController = new CartController();
    const [numberOfItems, setNumberOfItems] = useState(cartController.getNumberOfItems());

    const handleLogin = (event) => {
        console.log("Handle Login");
    }

    useEffect(() => { // TODO: update header
        
    }, [])

    return (
        <header className="default-header">
            <div className="header-logo">
                <img src={logo} alt="logo"/>
            </div>
            <div className="header-nav">
                <ul>
                    <li><ButtonLink name="Home" path="/" /></li>
                    <li><ButtonLink name="Products" path="/" /></li>
                    <li><Badge number={numberOfItems}><ButtonLink name="Cart" path="/Cart" /></Badge></li>
                    <li><ButtonLink name="Login" onClick={handleLogin} /></li>
                </ul>
            </div>
        </header>
    )
}
