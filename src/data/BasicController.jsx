export class BasicController{
    static instance;
    constructor(){
        if(BasicController.instance){
            return BasicController.instance
        }

        BasicController.instance = this;
    }
}