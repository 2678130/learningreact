import acImg from "../img/games/ac_lg.jpg";
import pcImg from "../img/games/cp2077_lg.jpg";
import ffImg from "../img/games/ffxv_lg.jpg";
import gfImg from "../img/games/gf_lg.jpg";
import gowImg from "../img/games/gow_lg.jpg";
import p5Img from "../img/games/p5_lg.jpg";

export class CartController {
    static instance;
    constructor() {
        if (CartController.instance) {
            return CartController.instance;
        }
        // callback functions subs
        this.subscribers = new Set();

        // key => productId data => cartItem
        this.cartItems = new Map();

        // products
        this.products = new Map();
        this.addProducts();


        // instantiate if not instantiated
        CartController.instance = this;
    }

    /******************************************
     * SUBSCRIPTIONS
     ******************************************/

    /**
     * 1. subscribe to cart
     * @param {function} subscriber 
     */
    subscribeToCart = (subscriber) => {
        this.subscribers.add(subscriber)
    }

    /**
     * 2. unsubscribe to cart
     * @param {function} subscriber
     */
    unsubscribeToCart = (subscriber) => {
        this.subscribers.delete(subscriber);
    }

    /**
     * -> invoke call back to let know update on cart
     */
    updateCart = () => {
        this.subscribers.forEach(fn => fn());
    }


    /*********************************
     * CART ITEMS
     *********************************/

    /**
     * -> Add item to cart
     * @param {object} addInfo
     */
    addItemToCart = (addInfo) => {
        const productId = addInfo.productId;
        const cartItem = this.products.get(productId);
        cartItem.qte = addInfo.qte;

        // would need some validation
        this.cartItems.set(productId, cartItem);
        this.updateCart();
    }

    /**
     * -> Remove item from cart
     * @param {string} itemId 
     */
    deleteCartItem = (itemId) => {
        this.cartItems.delete(itemId);
        this.updateCart();
    }

    /**
     * -> Increase item by one
     * @param {string} itemId 
     */
    increaseCartItem = (itemId) => {
        let item = this.cartItems.get(itemId);
        if (item) {
            item.qte = item.qte + 1;
        }
        this.updateCart();
    }

    /**
     * -> decrease item by one
     * @param {string} itemId 
     */
    decreaseCartItem = (itemId) => {
        let item = this.cartItems.get(itemId);
        if (item) {
            item.qte = Math.max(0, item.qte - 1);
        }
        this.updateCart();
    }

    /**
     * -> return array of all cart items
     * @returns {array}
     */
    getCartItems = () => {
        return Array.from(this.cartItems.values());
    }

    /**
     * -> return a single cart item
     * @param {string} itemId
     */
    getCartItem = (itemId) =>{
        return this.cartItems.get(itemId);
    }

    /**
     * 
     * -> get total amound in cart
     */
    getCartTotal = () => { //TODO cart total
        let total = 0;
        return total;
    }

    /**
     * -> return the number of items
     */
    getNumberOfItems = () => { // TODO cart item count
        let numberOfItems = 0;        
        return numberOfItems;
    }


    /***************************
     * Products
     ***************************/
    getProducts = () => {
        return Array.from(this.products.values());
    }

    addProducts = () => {
        this.products.set("ac", {
            productId: "ac",
            productName: "Assasins Creed",
            productPrice: 2599,
            img: acImg
        });
        this.products.set("cp", {
            productId: "cp",
            productName: "Cyberpunk 2077",
            productPrice: 6599,
            img: pcImg
        })
        this.products.set("ff", {
            productId: "ff",
            productName: "Final Fantasy XV",
            productPrice: 3599,
            img: ffImg
        })
        this.products.set("gf", {
            productId: "gf",
            productName: "Greed Fall",
            productPrice: 2199,
            img: gfImg
        })
        this.products.set("gow", {
            productId: "gow",
            productName: "God of War",
            productPrice: 5599,
            img: gowImg
        })
        this.products.set("p5", {
            productId: "p5",
            productName: "Persona 5",
            productPrice: 1599,
            img: p5Img
        })
    }
}